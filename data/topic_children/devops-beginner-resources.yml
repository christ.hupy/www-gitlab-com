title: Start your DevOps journey with these 11 helpful resources
description: get started with DevOps
header_body: Here are the blogs, videos, webcasts, and more to help you get
  started with DevOps.
canonical_path: /topics/ops/devops-beginner-resources/
file_name: devops-beginner-resources
parent_topic: ops
twitter_image: /images/opengraph/gitlab-blog-cover.png
body: >
  Since there are a lot of tools and terms to master, getting started in DevOps
  can be challenging. We’ve compiled a list of 11 useful and practical resources
  to help you quickly get up to speed.


  ## New to DevOps? Here’s what you need to know


  If you’re new to a DevOps team or consider yourself a DevOps beginner, we have a [guide that will help you get off the ground](https://learn.gitlab.com/beginners-guide-devops/guide-to-devops). This will help you understand what DevOps is all about, what terms, methodologies, and technologies you need to understand, and why collaboration is a key tenet that you will need to adopt. This guide also features an example of how DevOps is changing the game for one large financial investment bank. And it offers information on how working in DevOps can affect your career. 


  ## How to begin your DevOps journey


  You’ve started working in DevOps or you want to be in the DevOps field, so you’ve begun a journey into learning new skills and adopting a new mindset. Here we walk you through [how to take the first steps](/blog/2022/01/13/how-to-begin-your-devops-journey/) on this exciting new path.  



  ## GitLab’s guide to CI/CD for beginners


  Continuous integration and [continuous delivery](https://about.gitlab.com/direction/release/continuous_delivery/#continuous-delivery-vs-deployment) (known as CI/CD) are the cornerstones of DevOps. [Here’s what you need to know](/blog/2020/07/06/beginner-guide-ci-cd/)
   about CI/CD for beginners. And here’s a [video tutorial](https://www.youtube.com/watch?v=DWb1HNmbmeM) that will help you, too. 

  ## Your guide to learning about Git


  Whatever software you develop and whichever languages you use, you’ll soon run into Git, a source code management system that helps programmers work collaboratively. GitLab senior developer evangelist [Brendan O’Leary](/company/team/#brendan) walks you through [what you need to know](/blog/2020/04/13/beginner-git-guide/). 


  ## Understanding GitOps


  [GitOps is an important operational framework](/topics/gitops/) in DevOps, giving you a way to take best practices, like version control, compliance methodologies and CI/CD, and apply them to infrastructure automation and application deployment.


  To understand even more about GitOps and what it can do for your DevOps team, [check out this webcast](/why/gitops-infrastructure-automation/) of a panel discussion with pros from Weaveworks, HashiCorp, Red Hat, and GitLab talking about the future of infrastructure automation.


  ## Understanding DevSecOps


  The [practice of DevSecOps](/topics/devsecops/) - or development, security, and operations - focuses on integrating security into the DevOps lifecycle. It's an approach to culture, automation, and platform design that makes it a shared responsibility, among everyone on the team, to create code with security in mind. By factoring in security this way, it increases efficiency and deployment speed, while also preventing, catching and solving bugs and compliance issues before code goes into production. 


  For more information on DevSecOps, check out these [three best practices](/topics/devsecops/three-steps-to-better-devsecops/) for implementing better DevSecOps. And for [information on why developer-first security is important](/topics/devsecops/what-is-developer-first-security/), here’s more guidance for you. 


  Want to know more about how to shift left? [This webcast](/webcast/wishes-to-workflows/) will help you understand how to make it happen. 


  ## How to be a stand-out DevOps team


  There are several things you, and your teammates, can do to [make your DevOps team elite performers](/blog/2021/10/26/how-to-make-your-devops-team-elite-performers/). There’s a big difference between being an elite performer and low performers, affecting your speed to deployment, efficiency and your corporate agility. Check out the advantages, as well as tips on how to get there. 
   
  ## How documentation can unify projects and team efforts
   
  If you’re looking to figure out how to unify efforts between projects and DevOps teams, and to share specialized knowledge and guidance, you need to learn about documentation. This blog will walk you through [what documentation is all about](/blog/2022/01/11/16-ways-to-get-the-most-out-of-software-documentation/) and what it can do for your DevOps efforts.
