---
layout: markdown_page
title: "Product Direction - Monitor:Observability"
description: "A unified observability platform for your DevOps needs"
canonical_path: "/direction/monitor/observability"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for Monitor:Observability. If you'd like to discuss this direction
directly with the product managers for Monitor:Observability, feel free to reach out to Sebastien Pahl (Principal PM of Monitor:Observability) ([GitLab](https://gitlab.com/sebastianpahl), [Email](mailto:spahl@gitlab.com)).

## Vision
We aim to democratize access to observability tools in order to create a world where developers and operators can easily, and seemlessly collaborate to ensure production applications are availble and responsive to their users. 

## Newsworthy
On Dec 14, 2021, GitLab announced the [acquisition of Opstrace](https://about.gitlab.com/press/releases/2021-12-14-gitlab-acquires-opstrace-to-expand-its-devops-platform-with-open-source-observability-solution.html). Opstrace is an observability distribution that will become integrated and usable out-of-the-box for all GitLab users. Using Opstrace, users gain the benefit of having a full observability platform, starting with Metrics, powered by Prometheus. 

## Direction

Over the coming months, we will focus on integrating Opstrace into GitLab. Observability will be available, by default, for both GitLab SaaS and Self-Managed users, starting at the free tier.
Using GitLab’s observability capability, you will be using a completely open-sourced platform. You do not have to worry about vendor lock-in from instrumentation to alerting. 

Organizations often have the choice of using an observability vendor or building their own observability platform. With the former, teams outsource the problems of operating a system that needs to be scaled and consistently updated but are locked-in to proprietary software. With the latter, teams have to manage the complexity of an observability platform, and figure out how to make all the open-source components work together. We want that choice to be easy to make.

We are different because we won’t approach observability by reinventing the wheel with yet another observability storage backend. Rather, we will focus on making the toil of operating open-source observability tools easy. Instead of staffing a team to keep toolings up to date, you can easily and confidently upgrade with the observability distribution. Furthermore, this observability tool is integrated with the rest of GitLab so you have a single tool to build, test, collaborate, deploy, and monitor your applications.

Over time, we’ll add additional delightful experiences such as:

1. Add tracing to the Opstrace/GitLab stack. Tracing helps users understand the flow of requests and is particularly useful when you need to debug microservices-based applications.
1. Add logging to the Opstrace/GitLab stack. Aggregating logs in a single, searchable interface helps operators find the relevant log message quickly.
1. Cross-reference different observability data types. Making it easy to go from a metric to the related log messages or to jump from a long-running trace to the impacted metric, will help teams understand more holistically what is happening with their application.
1. Enable more collaboration for teams when triaging and investigating issues using observability data. Using observability tools has mostly been a siloed activity until now. We want to enable teams to be able to tag each other, and easily show observability data into GitLab issues and MRs, so that teams can collaborate more effectively without having to rely on synchronous communication.

## Principles
We'll pursue our vision based on the following principles:

### On by Default
We want developers to have immediate access to a robust observability platform as a first principle. That means we will ensure it is available by default, starts with convention over configuration and is easy to utilize. 

### Unified Data Store
We believe that observability data is most valuable when it is easy to glean insights across data types of metrics, logs, traces and errors. As a result we're working to store all types of observability data in a [unified datastore - Clickhouse](/handbook/engineering/development/ops/monitor/observability/#clickhouse-datastore).

### Integrated UI
A common interface, and most importantly a common collaboration space is key to bringing observability to where developers work. We'll create an [integrated UI](/handbook/engineering/development/ops/monitor/observability/#gitlab-observability-ui-previously-known-as-opstrace-ui) that allows for collaboration on observability data directly within GitLab.

## What’s next?
Now that Opstrace is part of GitLab, the first thing we’ll do is integrate Opstrace into GitLab. For additional details on the integration progress, follow this [GitLab epic](https://gitlab.com/groups/gitlab-org/-/epics/6976).

## Performance Indicators
The Monitor:Observability will work to help all GitLab customers Monitor their apps by providing a simple on-by-default observability stack. We'll focus first on more users, and more contributors' adoption in our [dual flywheel strategy](/company/strategy/#dual-flywheels). We'll know if we are trending in the right direction by looking at the Monitor:Observability Monthly Active Users adoption, which will grow if we are providing value to our users. 

## Market
TBD

### Scope
The Observability group will be focused on bringing observability, including Metrics, Tracing, and Logging to market within the GitLab platform.


