---
layout: handbook-page-toc
title: Manage Stage
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Manage
{: #welcome}

The responsibilities of this stage are described by the [Manage product
category](/handbook/product/categories/#manage-stage). Manage is made up of multiple groups, each with their own categories and areas of
responsibility.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the relevant Product Manager for the [category](/handbook/product/categories/#dev). GitLab employees can also use [#s_manage](https://gitlab.slack.com/messages/CBFCUM0RX).

## How we work

* In accordance with our [GitLab values](/handbook/values/).
* Transparently: nearly everything is public, we record/livestream meetings whenever possible.
* We get a chance to work on the things we want to work on.
* Everyone can contribute; no silos.
  * The goal is to have product give engineering and design the opportunity to be involved with direction and issue definition from the very beginning.

## Hiring

We use an [issue board](https://gitlab.com/gl-talent-acquisition/req-intake/-/boards/3479556?scope=all&label_name[]=devops%3A%3Amanage) to track open Development positions within the Manage stage.

1. When a new position is identified and approved, the hiring manager ("HM") will create a kickoff issue within the [req-intake project](https://gitlab.com/gl-talent-acquisition/req-intake) and assign it to themselves. This may result in duplicated kickoff issues if there are multiple hires approved. Due dates will be updated for the target hiring date so it's transparent when we are behind, and by how much.
1. New positions require a Greenhouse ID ("GHID") from Finance. If your position is still waiting on a GHID, use the ~"HM::Awaiting Greenhouse ID" label. If you already have the GHID for your position, add it to the issue description.
1. Before a position can be opened in Greenhouse, your recruiter will need some initial information about this position which is gathered inside a kickoff issue. You can fill this information out at any time between creation and receiving the GHID.
1. Sometimes there is a delay between completing the kickoff issue and creating the position in Greenhouse, while you and your recruiter align on requirements. Use the ~"HM::Completed Kickoff Issue" label to signify that the position is ready to go live in Greenhouse, and reassign it to the recruiter DRI.
1. When the position is accepting candidates in Greenhouse and the hiring process begins (sourcing candidates, reviewing resumes, scheduling interviews), update the label with ~"HM::Interviewing". More granular information, such as the number of screening calls or technical/behavioral interviews scheduled, can be reported weekly in 1-1's or using the issue. While many people are involved in this process, hiring managers are still the DRI for a successful hire.
1. Once you have selected a candidate for the position, you can use the ~"HM::Pending Offer" label to indicate the Justification and Offer process has been started.
1. If you become blocked at any point during this process (unable to get the GHID, waiting too long between stages, etc), use the ~"blocked" label and add a note to the issue about the problem you're experiencing.

### DRIs & SLOs

Note: SLO's are based on business hours

| Stage | DRI | SLO | Description |
| ------ | ------ | ------ | ------ |
| [Create kickoff issue](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/new) | Hiring Manager | | |
| Create requisition in Greenhouse | Recruiter | 48 hours | See [preparations](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/issues/317#preparations) |
| Review applications for qualified -> screening | Hiring manager | 36 hours | Hiring managers should determine new applications are qualified/rejected within 36 hours of submission |
| Screen candidates | Hiring manager | 1 week | Screening calls should be scheduled within 1 week of moving a candidate to this stage |
| Technical interviews | Hiring manager| 1 week | Technical interviews should be requested for scheduling within 1 week of moving a candidate to this stage. [Process defined here](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/issues/317#team-interviews) |
| Final round interviews | Recruiter | | |
| Justification | Hiring manager | | |
| Reference checks | Hiring manager | | |
| Extend an offer | Recruiter | | |

### Escalation process

Hiring managers should be checking the progress of their positions on a daily basis in order to understand when an escalation may be needed. To escalate a problem or candidate:
1. Create a Slack channel under "hire-XXXX" where XXXX is either the candidates initials or the requisition ID
1. Include your manager, `@urselak`, and `@leif`
From here, we can pull in additional help to resolve the escalation and archive the channel.

**_Reminder_**: Please do not use a candidate's full name in a public Slack channel.


## Direction

The direction and strategy for Manage is documented on [https://about.gitlab.com/direction/manage/](https://about.gitlab.com/direction/manage/). This page (and the category direction
pages under the "Categories" header) is the single source of truth on where we're going and why.
* Direction pages should be reviewed regularly by Product. When updating these pages, please CC the relevant group to keep your teammates informed.
* Product should make sure that their groups understand the direction and have an opportunity to contribute to it. Consider a monthly direction AMA for your group to field questions.


## Career Development

### Giving / receiving feedback

It can be hard to understand how you're doing in your role, because feedback can come off as formal (annual reviews, 360 surveys, career development conversations, goal check-ins) or casual (in Slack channels, 1-1's, MR reviews, team meetings.) We receive various kinds of feedback regularly and through different formats, so the type of feedback you're receiving is not always clear. In order to be more intentional about the types of feedback given, here is a classification chart based on [three types of feedback](https://forimpact.org/three-types-feedback/#:~:text=%E2%80%9CFeedback%20comes%20in%20three%20forms,about%20relationship%20and%20human%20connection.):

| Label | Meaning | Example |
| **(appreciation)** | I want to thank you for doing this, and please do more of it in the future | "I did not expect that you would have created a working group, because you've done so, our whole team will benefit from the results."
| **(coaching)** | I'm trying to help you improve a behavior you are already exhibiting or change a behavior that you currently have | "The reports that you give me are very helpful, and in the future we can schedule them for the first of the month to be more consistent."
| **(evaluation)** | Tells you where you stand according to existing standards or expectations | "My expectation was that our decision would be transparent. Since it was not, our team has forgotten the decision, so we must be sure and meet that expectation next time." |

## Meetings

Although we have a bias for asynchronous communication, synchronous meetings are necessary and should adhere to our [communication guidelines](/handbook/communication/#video-calls). Some regular meetings that take place in Manage are:

| Frequency | Meeting                              | DRI         | Possible topics                                                                                        |
|-----------|--------------------------------------|-------------|--------------------------------------------------------------------------------------------------------|
| Every 2 weeks | Stage-level Product/Engineering  | @ogolowinski          | Collaborate across groups on specific issues, discuss global stage-level optimization (process, people)   |
| Monthly   | Stage-level meeting (Manage Monthly) | @ogolowinski    | Stage-level news, objectives, accomplishments                                                          |
| Monthly   | Stage-level social call              | @dennis     | Getting to know each other                                                                             |


For one-off, topic specific meetings, please always consider recording these calls and sharing them (or taking notes in a [publicly available document](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit)).

Agenda documents and recordings can be placed in the [shared Google drive](https://drive.google.com/drive/u/0/folders/0ALpc3GhrDkKwUk9PVA) (internal only) as a single source of truth.

Meetings that are not 1:1s or covering confidential topics should be added to the Manage Shared calendar.

All meetings should have an agenda prepared at least 12 hours in advance. If this is not the case, you are not obligated to attend the meeting. Consider meetings canceled if they do not have an agenda by the start time of the meeting.

## Shared calendar

* To add the shared calendar to your Google Calendar, please use this [link](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) (GitLab internal).
* To add a meeting to the shared calendar, please add [the link in this document](https://docs.google.com/document/d/1IxGuORI-vfVd6irNdUwpnOBZDWALWzOqhQzC9E39ixQ/edit) to the event.
* To add a new member to the shared calendar
  * Click "Settings and Sharing" in the kebab menu when mousing over "Manage Shared" in your Google Calendar sidebar under "My calendars".
  * Scroll to the "Share with specific people" section of the settings area. Click "Add people" and add the new member with "Make changes and manage sharing".
* For a more detailed walkthrough, have a look at a quick [video walkthrough](https://www.youtube.com/watch?v=TmcPuuljf1w)


## Dashboards

- [Stage Development Metrics](https://app.periscopedata.com/app/gitlab/855124/Manage:Development-Metrics)
- [Customer priorities](https://app.periscopedata.com/app/gitlab/882533/Dev:-Manage-Customer-Priorities)
- [Performance indicator overview](/handbook/engineering/metrics/dev/manage/)

## Holiday Gift Exchange (2021)

As a globally distributed team who has not had an opportunity to interact face-to-face in quite some time due to the pandemic, we must find ways to be creative and come together as one unit!

We will kick off the holiday season on November 22 by _optionally_ participating in a Secret Santa exchange - an exchange where a group of colleagues will exchange holiday presents anonymously with each member of the group being assigned to another member to provide a small gift.
1. The exchange amount will be $30 USD, including shipping and tax fees. This amount cannot be expensed and would need to be paid for out of pocket. Team members are not required to participate.
1. A form will be provided on November 22 to sign up. This form will include ideal gift ideas for yourself, as well as your formatted mailing address, and local / online stores to choose your gift from.
1. The form will close on December 8. At this time, your Secret Santa will be randomly selected and you will be given the name of a team member to buy a gift for, their ideal gift idea and local buying options if available.

## Links and resources
{: #links}

<%= partial("handbook/engineering/development/dev/manage/shared_links.erb") %>
* Our handbook pages
  * [Authentication & Authorization](/handbook/engineering/development/dev/manage/authentication-and-authorization/)
  * [Compliance](/handbook/engineering/development/dev/manage/compliance/)
  * [Import](/handbook/engineering/development/dev/manage/import/)
  * [Optimize](/handbook/engineering/development/dev/manage/optimize/)
  * [Workspace](/handbook/engineering/development/dev/manage/workspace/)
* Our Slack channels
  * Manage:Authentication and Authorization [#g_manage_auth](https://gitlab.slack.com/archives/CLM1D8QR0)
  * Manage:Optimize [#g_manage_optimize](https://gitlab.slack.com/messages/CJZR6KPB4)
  * Manage:Compliance [#g_manage_compliance](https://gitlab.slack.com/messages/CN7C8029H)
  * Manage:Import [#g_manage_import](https://gitlab.slack.com/messages/CLX7WMSKW)
  * Manage:Workspace [#g_manage_workspace](https://gitlab.slack.com/archives/C02AZ7C32N5)
* Issue boards
  * Authentication and Authorization [build board](https://gitlab.com/groups/gitlab-org/-/boards/1305005) and [refinement board](https://gitlab.com/groups/gitlab-org/-/boards/1747837)
  * Compliance [build board](https://gitlab.com/groups/gitlab-org/-/boards/1305010) and [refinement board](https://gitlab.com/groups/gitlab-org/-/boards/2600795)
  * Import [refinement and build board](https://gitlab.com/groups/gitlab-org/-/boards/2013571)
  * Optimize [build board](https://gitlab.com/groups/gitlab-org/-/boards/1401511) and [refinement board](https://gitlab.com/groups/gitlab-org/-/boards/1874426)
  * Workspace placeholder
