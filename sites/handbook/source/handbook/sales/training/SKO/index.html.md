---
layout: handbook-page-toc
title: "Sales Kickoff"
description: "The GitLab Sales Kickoff sets the tone for the GitLab field organization for the new fiscal year"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Kickoff (SKO) Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

# [Sales Kickoff 2022](/handbook/sales/training/SKO/2022)
GitLab SKO 2022 will take place virtually from March 15-17, 2022. The virtual event will consist of three half-days offered in two time zone options to accommodate our team members around the world. To learn more about the event, please visit the [Sales Kickoff 2002 Handbook page](/handbook/sales/training/SKO/2022) and checkout the [SKO 2022 Field Team FAQ document](https://docs.google.com/document/d/1OTRQjtWs-8sAnZG_ITQwNn0B2bZ6ed6sqhDEWKlimqE/edit#) (GitLab internal only). 

----

## [Sales Kickoff 2020](/handbook/sales/training/SKO/2020)

## [Sales Kickoff 2021](/handbook/sales/training/SKO/2021)

----

## Sales Kickoff Planning 

For more information about the Sales Kickoff planning core team and process, see the [Sales Kickoff Planning](https://about.gitlab.com/handbook/sales/training/SKO/SKO-planning/) page.
