---
layout: handbook-page-toc
title: "Focus Account Lists"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## [This page has moved](/handbook/marketing/account-based-marketing/key-account-lists/)
Please update any links where this page is currently referenced.
