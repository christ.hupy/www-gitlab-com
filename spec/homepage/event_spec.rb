describe Gitlab::Homepage::Event do
  subject(:event) { described_class.new(data) }
  let(:data) do
    {
      'topic' => 'Conference',
      'date_starts' => 'Aug 30, 2020',
      'date_ends' => 'Sept 1, 2020'
    }
  end

  describe '#sort_date' do
    subject { event.sort_date }

    it { is_expected.to eq(Date.new(2020, 8, 30)) }

    context 'August 30, 2021' do
      let(:data) { super().merge('date_starts' => 'August 30, 2021') }

      it { is_expected.to eq(Date.new(2021, 8, 30)) }
    end

    context 'February 18, 2020' do
      let(:data) { super().merge('date_starts' => 'February 18, 2020') }

      it { is_expected.to eq(Date.new(2020, 2, 18)) }
    end
  end

  describe '.all!' do
    subject { described_class.all! }

    before do
      allow(YAML).to receive(:load_file) do
        [
          {
            'topic' => 'Conf 1',
            'date_starts' => 'Feb 2, 2019',
            'date_ends' => 'Feb 2, 2019'
          },
          {
            'topic' => 'Conf 2',
            'date_starts' => 'Feb 1, 2019',
            'date_ends' => 'Feb 2, 2019'
          },
          {
            'topic' => 'Conf 3',
            'date_starts' => 'Jan 30, 2019',
            'date_ends' => 'Feb 2, 2019'
          }
        ]
      end
    end

    it 'returns Event objects' do
      events = subject

      expect(events.count).to eq(3)
      expect(events.map(&:topic)).to match_array(['Conf 1', 'Conf 2', 'Conf 3'])
    end

    context 'Sorting' do
      it 'sorts events by starting date' do
        sorted_events = subject.sort

        expect(sorted_events.map(&:topic)).to eq(['Conf 3', 'Conf 2', 'Conf 1'])
      end
    end
  end
end
